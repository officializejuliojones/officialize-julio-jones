# Officialize - Julio Jones

The main objective of this project is for teams of students to pitch, design and develop a
professional single page site highlighting a selected player and their brand. The site must
include but is not limited to the following sections: player bio, player history/timeline, promo
video. Each team will be assigned one of the five listed athletes; Eddie Lacy, Julio Jones,
Melvin Gordon, Odell Beckham Jr, Richard Sherman. 

##Prerequisites

If you do not have SASS installed in your computer, you will need to install SASS first.

###Install and run SASS

- check the links below in order to install SASS
'http://sass-lang.com/install'
'https://goo.gl/tkHZBp'
- everytime you will need make SASS watch your SCSS files and make it compile the CSS. 
- to do so, go to Terminal or CMD and type in `sass --watch scss:css`.

###Using Gulp to watch SASS (RECOMMENDED WAY: why? check [Gulp for Beginners](https://css-tricks.com/gulp-for-beginners/))

- before install gulp, you will need to install [node.js](https://nodejs.org/en/download/)
- go to Terminal/Git Bash and hit `npm install`
- then type in `npm -v` to make sure it shows the version of npm you have installed
- then install gulp. hit `npm install gulp -g` in Terminal/Git Bash (install gulp globally)
- then type in `gulp -v` to make sure it shows the version of gulp you have installed
- BELOW ARE THE STEPS YOU NEED TO DO EVERYTIME YOU WORK ON A NEW PROJECT (creating gulp locally)
- in Terminal/Git Bash, go to your working directory
- type `npm init` in Terminal/Git Bash
- the last step will creat a json file for you
- then follow the steps to set up the json file (you just type in some project information)
- then you will need to type `npm install gulp --save-dev` and `npm install gulp-sass --save-dev`
- then create a file called gulpfile.js in your project root folder
- write gulp task in gulpfile.js
- you can find gulp watch sass task in [Getting started with Gulp and Sass](http://ryanchristiani.com/getting-started-with-gulp-and-sass/)  and [Gulp for Beginners](https://css-tricks.com/gulp-for-beginners/). please read both posts to learn how to do all these steps above

##Running Test

- open Terminal/GitBash
- Navigate to MAMP/WAMP htdocs folder
- type in `git clone https://bitbucket.org/officializejuliojones/officialize-julio-jones`
- start MAMP/WAMP server
- open the site in MAMP/WAMP local host
- inspect the website to see if it works properly and is responsive


##Built with

- Foundation 6
- Sass
- Gulp
- [Slick Carousel](http://kenwheeler.github.io/slick/)

##Authors

- Adrian Zhao
- Fran Wyllie
- Julie Couwenberghs
- Shan Jiang
- Xin Jin