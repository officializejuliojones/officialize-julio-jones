$(document).ready(function () {
    $('.slick_slider').slick({
        infinite: false,
        dots: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
        prevArrow: "<div class='prev_arrow'><span class='fa fa-chevron-left'></span></div>",
        nextArrow: "<div class='next_arrow'><span class='fa fa-chevron-right'></span></div>"
    });
});