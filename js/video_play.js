(function () {
    var play = document.querySelectorAll('.video_play'),
        video = document.querySelectorAll('.timeline_video');

    function videoPlay(event) {
        var videoItem = event.currentTarget.parentNode.previousElementSibling;
        if (videoItem.paused) {
            videoItem.play();
            this.classList.remove('fa-play-circle');
            this.classList.add('fa-pause-circle');
        } else {
            videoItem.pause();
            this.classList.add('fa-play-circle');
            this.classList.remove('fa-pause-circle');
        }
    }

    function soundOn() {
        if (window.screen.width >= 1024) {
            this.muted = false;
        } else {
            this.muted = true;
        }
    }

    function soundOff() {
        this.muted = true;
    }

    play.forEach(function (playItem) {
        playItem.addEventListener('click', videoPlay, false);
    });
    video.forEach(function (videoItem) {
        videoItem.addEventListener('mouseover', soundOn, false);
    });
    video.forEach(function (videoItem) {
        videoItem.addEventListener('mouseleave', soundOff, false);
    });
})();