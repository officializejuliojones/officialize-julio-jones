(function () {
    var more = document.querySelectorAll('.read_more'),
        less = document.querySelectorAll('.show_less');

    function readMore() {
        this.parentElement.classList.toggle('read_more_active');
    }

    more.forEach(function (moreItem) {
        moreItem.addEventListener('click', readMore, false);
    });
    less.forEach(function (lessItem) {
        lessItem.addEventListener('click', readMore, false);
    })
})();