$(document).foundation();
$(document).ready(function () {
    var backToTop = document.querySelector('.back_to_top_wrap'),
        navBar = document.querySelector('.head_nav_wrap'),
        logo = document.querySelector('.julio_logo'),
        deActive = document.querySelectorAll('.de-active'),
        overlay = document.querySelector('.overlay'),
        burgerButton = document.querySelector('.burger_button');

    function showButton() {
        if (window.pageYOffset > 100) {
            backToTop.style.opacity = 1;
            navBar.classList.add('shrink');
            logo.classList.add('shrink');
        } else {
            backToTop.style.opacity = 0;
            navBar.classList.remove('shrink');
            logo.classList.remove('shrink');
        }
    }

    function hideMenu() {
        overlay.classList.toggle('show');
        burgerButton.classList.toggle('active');
    }

    window.addEventListener('scroll', showButton, false);
    deActive.forEach(function (menu) {
        menu.addEventListener('click', hideMenu, false);
    });
});