$(document).ready(function(){
					 
	$(".getDataBtns").click(function(e){
		var currentBtn = $(this).attr("id");
		
		if(currentBtn == "getAge"){
			$.get("twitterData.php", function(data){
				var dataJSON = JSON.parse(data);
				console.log(dataJSON.length);
				var ageJunior = 0;
				var ageMiddle = 0;
				var ageSenior = 0;
				var ageElder = 0;
				for(var i = 0; i < dataJSON.length; i++){
					if((dataJSON[i]["user_age"]) >= 20 && (dataJSON[i]["user_age"]) < 30){
						ageJunior++;
					}
					if((dataJSON[i]["user_age"]) >= 30 && (dataJSON[i]["user_age"]) < 40){
						ageMiddle++;
					}
					if((dataJSON[i]["user_age"]) >= 40 && (dataJSON[i]["user_age"]) < 49){
						ageSenior++;
					}
					if((dataJSON[i]["user_age"]) >= 50 && (dataJSON[i]["user_age"]) < 60){
						ageElder++;
					}				
				}
				google.charts.load('current', {packages: ['corechart']}); 
				google.charts.setOnLoadCallback(drawChart); 
				function drawChart() {
				  var data = new google.visualization.DataTable();
				  data.addColumn('string', 'Age'); //a column value type is string
				  data.addColumn('number', 'amount of users'); //another column value type is number
				  data.addRows([ //add value to datatable
					['20 - 29', ageJunior],
					['30 - 39', ageMiddle],
					['40 -49', ageSenior], 
					['50 -59', ageElder]
				  ]);
				
				var options = {'title':'Followers age',
								 'width':400,
								 'height':300};		
								 
				var chart = new google.visualization.PieChart(document.getElementById('newChart')); 
				chart.draw(data, options);
				}
			});
			e.preventDefault();
		}
		else if(currentBtn == "getGender"){
			$.get("twitterData.php", function(data){
				var dataJSON = JSON.parse(data);
				console.log(dataJSON.length);
				var male = 0;
				var female = 0;
				for(var i = 0; i < dataJSON.length; i++){
					if((dataJSON[i]["user_gender"]) == "Male"){
						male++;
					}
					else{
						female++;
					}
				}
				google.charts.load('current', {packages: ['corechart']}); 
				google.charts.setOnLoadCallback(drawChart); 
				function drawChart() {
				  var data = new google.visualization.DataTable();
				  data.addColumn('string', 'gender'); //a column value type is string
				  data.addColumn('number', 'number of users'); //another column value type is number
				  data.addRows([ //add value to datatable
					['Male', male],
					['Female', female]
				  ]);
				
				var options = {'title':'Followers gender',
								 'width': 600,
								 'height': 300};		
								 
				var chart = new google.visualization.ColumnChart(document.getElementById('newChart')); 
				chart.draw(data, options);
				}
			});
			e.preventDefault();			
		}
	});	
});