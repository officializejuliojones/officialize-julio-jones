$(document).ready(function(){
	$.get("dataFromTwitter.php", function(data){
		var twitterContent = JSON.parse(data);
		var tweetTable = "";
		for(var i = 0; i < 5; i++){
			tweetTable += "<div class='tweets' style='background: #fff; opacity: 0.8; padding: 10px; font-size: 100%; margin: 10px 0'>" + "<p style='margin: 0; font-weight: 600'>" + twitterContent[i]["text"] + "</p><p style='text-align: right; margin: 0; font-weight: 600'>" + twitterContent[i]["created_at"] + "</p>" + "</div>";
		}
		console.log(twitterContent);
		$(".news_content").html(tweetTable);
	});
});